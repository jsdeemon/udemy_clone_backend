## Udemy clone backend / Django
### Setting up Django backend 

```bash
$ virtualenv venv 
$ source venv/bin/activate 
$ pip install -r requirements.txt
```

```bash
$ python3 manage.py makemigrations 
$ python3 manage.py migrate 
$ python3 manage.py createsuperuser 
```
email: dukenukem@email.cn 
name: duke3d
password: duke3d23@fg5 

http://localhost:8000/admin 


frontend 
https://github.com/OtchereDev/Udemy_Nextjs 

Full course:
https://www.youtube.com/watch?v=vb5fSdmafMM 

Udemy clone with ReactJs: 
https://www.youtube.com/watch?v=qiii35VZvBE